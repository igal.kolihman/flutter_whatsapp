import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whatsapp/models/user.dart';
import 'package:whatsapp/screens/wrapper.dart';
import 'package:whatsapp/services/auth.dart';

void main() => runApp(MyApp());

Map<int, Color> color = {
  50: Color.fromRGBO(0, 151, 136, 1),
  100: Color.fromRGBO(0, 151, 136, 1),
  200: Color.fromRGBO(0, 151, 136, 1),
  300: Color.fromRGBO(0, 151, 136, 1),
  400: Color.fromRGBO(0, 151, 136, 1),
  500: Color.fromRGBO(0, 151, 136, 1),
  600: Color.fromRGBO(0, 151, 136, 1),
  700: Color.fromRGBO(0, 151, 136, 1),
  800: Color.fromRGBO(0, 151, 136, 1),
  900: Color.fromRGBO(0, 151, 136, 1),
};

MaterialColor colorCustom = MaterialColor(0xFF009788, color);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        title: 'WhatsApp',
        theme: ThemeData(
          primarySwatch: colorCustom,
        ),
        home: Wrapper(),
      ),
    );
  }
}
