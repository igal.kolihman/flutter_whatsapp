import 'package:flutter/material.dart';
import 'package:whatsapp/screens/chats/group_chat_screen.dart';

const double imageRadius = 30;

class ChatsPage extends StatefulWidget {
  ChatsPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ChatsPageState createState() => _ChatsPageState();
}

class _ChatsPageState extends State<ChatsPage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search, color: Colors.white, size: 30),
              onPressed: null),
          IconButton(
              icon: Icon(Icons.more_vert, color: Colors.white, size: 30),
              onPressed: null)
        ],
      ),
      body: ListView(
        children: <Widget>[
          SingleChatItem(
            chatName: "Bill Gates",
            lastMassageStatus: 3,
            lastMessageTime: "20:35",
            lastMessage: "For the last time!!! I'm not Bill Gates!!",
            imagePath:
                "https://amp.businessinsider.com/images/5774622c4321f122008b5238-750-563.jpg",
          ),
          SingleChatItem(
            chatName: "Hulk",
            lastMassageStatus: 2,
            lastMessageTime: "11:05",
            lastMessage: "HULK SMASHHHHHHH 👿",
            imagePath:
                "https://media.comicbook.com/2019/12/marvel-hulk-1200939-1280x0.jpeg",
          ),
          SingleChatItem(
            chatName: "Iron Man",
            lastMassageStatus: 1,
            lastMessageTime: "19:20",
            lastMessage: "im rich af 💵💵💵💵💵💵",
            imagePath:
                "https://abcnews4.com/resources/media/eb47ede3-06b9-429c-8fef-9248c33a26ad-large16x9_AP18094578299737.jpg?1559758724342",
          ),
          SingleChatItem(
            chatName: "Captain America 🏈",
            lastMassageStatus: 3,
            lastMessageTime: "14:15",
            lastMessage: "I know how to give good speaches!",
            imagePath:
                "https://data.junkee.com/wp-content/uploads/2018/10/Chris-Evans-Captain-America.jpg",
          ),
          SingleChatItem(
            chatName: "Natasha",
            lastMassageStatus: 0,
            lastMessageTime: "6:35",
            lastMessage: "Gde Moi Tapachki?!?!?! ",
            imagePath:
                "https://images-na.ssl-images-amazon.com/images/I/61aRdEno1XL._SY355_.jpg",
          ),
          SingleChatItem(
            chatName: "Hawkeye",
            lastMassageStatus: 3,
            lastMessageTime: "8:35",
            lastMessage: "I dont know why im here 🤔",
            imagePath:
                "https://img.cinemablend.com/filter:scale/quill/8/c/6/b/9/5/8c6b956929470355d6a16667d9a515fb23c110a9.jpg?mw=600",
          ),
          SingleChatItem(
            chatName: "Spiderman 🕷",
            lastMassageStatus: 0,
            lastMessageTime: "20:00",
            lastMessage: "I'm late for school! talk to you later.",
            imagePath:
                "https://am23.mediaite.com/tms/cnt/uploads/2019/09/Ned-Was-Originally-in-the-Mid-Credit-Scenes-for-Far-from-Home-1200x675.jpg",
          ),
          SingleChatItem(
            chatName: "Thanos",
            lastMassageStatus: 2,
            lastMessageTime: "12:35",
            lastMessage: "I'm angry because i cant grow hair 👱🏼‍♂️",
            imagePath:
                "https://cdn.vox-cdn.com/thumbor/JBJzwCXmTJs0NgnFtSPm_f5SMyw=/0x0:2000x1000/1200x800/filters:focal(654x138:974x458)/cdn.vox-cdn.com/uploads/chorus_image/image/59408999/Thanos_MCU.0.jpg",
          ),
          SingleChatItem(
              chatName: "Ant Man",
              lastMassageStatus: 3,
              lastMessageTime: "12:35",
              lastMessage: "I get small. very very small",
              imagePath:
                  "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/ant-man-avengers-endgame-paul-rudd-1555594407.jpg?crop=1xw:1xh;center,top&resize=480:*"),
          Container(
            height: 70,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'New Chat',
        child: Icon(Icons.chat),
      ),
    ));
  }
}

class SingleChatItem extends StatelessWidget {
  SingleChatItem(
      {Key key,
      this.chatName,
      this.lastMessage,
      this.lastMessageTime,
      this.imagePath,
      this.lastMassageStatus})
      : super(key: key);

  final String chatName;
  final String lastMessage;
  final String lastMessageTime;
  final String imagePath;
  final int lastMassageStatus;
  final double iconSize = 16.0;

  Widget _lastMessageStatusIcon() {
    switch (this.lastMassageStatus) {
      case 0:
        {
          return Icon(
            Icons.access_time,
            color: Colors.grey,
            size: this.iconSize,
          );
        }
        break;
      case 1:
        {
          return Icon(
            Icons.done,
            color: Colors.grey,
            size: this.iconSize,
          );
        }
        break;
      case 2:
        {
          return Icon(
            Icons.done_all,
            color: Colors.grey,
            size: this.iconSize,
          );
        }
        break;
      case 3:
        {
          return Icon(
            Icons.done_all,
            color: Colors.lightBlue[300],
            size: this.iconSize,
          );
        }
        break;

      default:
        {
          return Icon(
            Icons.account_box,
            color: Colors.red,
            size: this.iconSize,
          );
        }
    }
  }

  void openGroupPage(BuildContext context) {
    Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new GroupChatPage()));
  }

  @override
  Widget build(BuildContext context) {
    // IconData iconData = Icons.access_time;
    // Color iconColor = Colors.grey;

    return Container(
        child: Column(
      children: <Widget>[
        ListTile(
          title: Text(
            this.chatName,
            textAlign: TextAlign.left,
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.black, fontSize: 16),
          ),
          subtitle: Container(
            child: Row(
              children: <Widget>[
                this._lastMessageStatusIcon(),
                Flexible(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(left: 2.0),
                    child: Text(this.lastMessage,
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        style: TextStyle(color: Colors.grey)),
                  ),
                ),
              ],
            ),
          ),
          leading: CircleAvatar(
            radius: imageRadius,
            backgroundImage: NetworkImage(this.imagePath),
          ),
          trailing: Column(
            children: <Widget>[
              Text(
                this.lastMessageTime,
                style: TextStyle(color: Colors.grey),
              )
            ],
          ),
          onTap: () {
            this.openGroupPage(context);
          },
        ),
        Divider(
          indent: imageRadius * 3,
        )
      ],
    ));
  }
}
