import 'dart:math';

import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';

class GroupChatPage extends StatefulWidget {
  GroupChatPage({Key key, this.title, this.participants, this.image})
      : super(key: key);

  final String title;
  final List participants;
  final String image;

  @override
  _GroupChatPageState createState() => _GroupChatPageState();
}

class _GroupChatPageState extends State<GroupChatPage> {
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        title: Text("Group Chat Screen"),
        actions: <Widget>[
          Transform.rotate(
            angle: -pi / 4,
            child: IconButton(
                icon: Icon(Icons.attach_file, color: Colors.white, size: 30),
                onPressed: null),
          ),
          IconButton(
              icon: Icon(Icons.more_vert, color: Colors.white, size: 30),
              onPressed: null)
        ],
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 10,
            child: Container(
              child: ListView(
                children: <Widget>[
                  DateBubble(date: "11/03/2020"),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  DateBubble(date: "YESTERDAY"),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                  DateBubble(date: "TODAY"),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  PersonalMessage(
                    message: "Hello World!",
                    time: "21:08",
                  ),
                  FriendMessage(
                    friendName: "George",
                    message: "I'm a friend! how are you doing",
                    time: "21:23",
                  ),
                ],
              ),
            ),
          ),
          Flexible(
              flex: 1,
              child: Container(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: EditorPanel())))
        ],
      ),
      backgroundColor: Color.fromRGBO(230, 225, 219, 1.0),
    ));
  }
}

class PersonalMessage extends StatelessWidget {
  PersonalMessage({
    Key key,
    this.message,
    this.time,
  }) : super(key: key);

  final String message;
  final String time;

  @override
  Widget build(BuildContext context) {
    return Bubble(
      margin: BubbleEdges.only(top: 3, left: 10, right: 10),
      color: Color.fromRGBO(225, 255, 199, 1.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Container(
            child: Text(
              this.message,
            ),
          ),
          Container(
            // alignment: Alignment.centerRight,
            child: Text(
              this.time,
              textAlign: TextAlign.right,
              style: TextStyle(fontSize: 10, color: Colors.grey),
            ),
          ),
        ],
      ),
      alignment: Alignment.topRight,
      nip: BubbleNip.rightTop,
    );
  }
}

class FriendMessage extends StatelessWidget {
  FriendMessage({
    Key key,
    this.friendName,
    this.message,
    this.time,
  }) : super(key: key);

  final String friendName;
  final String message;
  final String time;

  @override
  Widget build(BuildContext context) {
    return Bubble(
      margin: BubbleEdges.only(top: 3, left: 10, right: 10),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Text(
              this.friendName,
              style:
                  TextStyle(color: Colors.indigo, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            child: Text(
              this.message,
            ),
          ),
          Container(
            // alignment: Alignment.centerRight,
            child: Text(
              this.time,
              textAlign: TextAlign.left,
              style: TextStyle(fontSize: 10, color: Colors.grey),
            ),
          ),
        ],
      ),
      alignment: Alignment.topLeft,
      nip: BubbleNip.leftTop,
    );
  }
}

class DateBubble extends StatelessWidget {
  DateBubble({Key key, this.date}) : super(key: key);

  final String date;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10),
      child: Bubble(
        alignment: Alignment.center,
        color: Color.fromRGBO(212, 234, 244, 1.0),
        child: Text(this.date,
            textAlign: TextAlign.center, style: TextStyle(fontSize: 11.0)),
      ),
    );
  }
}

class EditorPanel extends StatelessWidget {
  EditorPanel({Key key}) : super(key: key);

  double cardRadius = 10.0;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Flexible(
            flex: 5,
            child: Card(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(45)),
              margin: EdgeInsets.only(left: 5, right: 5),
              child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Row(
                    children: <Widget>[
                      Icon(Icons.sentiment_very_satisfied, color: Colors.grey),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: 10, right: 10, bottom: 2, top: 2),
                          child: TextField(
                            minLines: 1,
                            maxLines: 8,
                            decoration: InputDecoration.collapsed(
                                hintText: "Type a message"),
                          ),
                        ),
                      ),
                      Transform.rotate(
                        angle: -pi / 4,
                        child: IconButton(
                            icon: Icon(Icons.attach_file, color: Colors.grey),
                            onPressed: null),
                      ),
                      Icon(Icons.camera_alt, color: Colors.grey),
                    ],
                  )),
            )),
        Flexible(
            flex: 1,
            child: FittedBox(
              fit: BoxFit.contain,
              child: FloatingActionButton(
                onPressed: null,
                child: Icon(Icons.mic),
                backgroundColor: Colors.green,
              ),
            ))
      ],
    );
  }
}
