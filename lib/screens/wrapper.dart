import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:whatsapp/models/user.dart';
import 'package:whatsapp/screens/authenticate/authenticate.dart';
import 'package:whatsapp/screens/home/home.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    print(user);

    // Return either Home or Authenticate wiget
    if (user == null) {
      return Authenticate();
    } else {
      return Home();
    }
  }
}
