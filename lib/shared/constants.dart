import 'package:flutter/material.dart';

Map<int, Color> color = {
  50: Color.fromRGBO(0, 151, 136, 1),
  100: Color.fromRGBO(0, 151, 136, 1),
  200: Color.fromRGBO(0, 151, 136, 1),
  300: Color.fromRGBO(0, 151, 136, 1),
  400: Color.fromRGBO(0, 151, 136, 1),
  500: Color.fromRGBO(0, 151, 136, 1),
  600: Color.fromRGBO(0, 151, 136, 1),
  700: Color.fromRGBO(0, 151, 136, 1),
  800: Color.fromRGBO(0, 151, 136, 1),
  900: Color.fromRGBO(0, 151, 136, 1),
};

MaterialColor colorCustom = MaterialColor(0xFF009788, color);

const int appColor = 0xFF009788;

const textInputDecoration = InputDecoration(
    fillColor: Colors.white,
    filled: true,
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.lightBlue, width: 2)),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Color(appColor), width: 2)));
